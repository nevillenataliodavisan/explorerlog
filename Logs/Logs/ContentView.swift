//
//  ContentView.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 27/04/21.
//

import SwiftUI

struct Myself: View {
    
    @State var interest1:String = ""
    @State var contactInformation1: String = ""
    @State var contactInformation2: String = ""
    @State var sosmed:String = ""
    @State var sosmed1:String = ""
    @State var number:Int = 1
    @State var save = false
    @State var otherInterest: String = ""
    @State var aboutMe:String = ""
    var body: some View {
        NavigationView{
            
            Form {
                Image("DOG")
                    .resizable()
                    .scaledToFit()
                
                Section(header: Text("Interest")){
                    TextField("Interest", text: $interest1)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                    TextField("Others", text: $otherInterest)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                }
                Section(header: Text("Contact Information")){
                    TextField("Whatsapp", text: $contactInformation1)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                    TextField("Line", text: $contactInformation2)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                }
                Section(header: Text("Social Media")){
                    TextField("Instagram", text: $sosmed)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                    TextField("Others", text: $sosmed1)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                }
                Section(header: Text("About Me")){
                    TextField("About Me", text: $aboutMe)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                }
                
                
                Stepper(value: $number, in: 1...10){
                    Text("On the scale of 1 to 10, how approachable are you? \(number)")
                }
                Button(action: {self.save.toggle()}){
                    Text("Save")
                }
                .alert(isPresented: $save) {
                            Alert(title: Text("Done"),
                                  message: Text("Thank you."),
                                  dismissButton: .default(Text("OK")))
            
            }
            
        }.navigationTitle("Myself")
            .toolbar{
                Button(action: {self.save.toggle()}){
                    Text("Save")
                }
                .alert(isPresented: $save) {
                            Alert(title: Text("Done"),
                                  message: Text("Thank you."),
                                  dismissButton: .default(Text("OK")))
            
                    }
                }
            }
    }
}
    
    

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        Myself()
        }
    }

