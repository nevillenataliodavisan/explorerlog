//
//  APIII2.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 05/05/21.
//

import SwiftUI

struct APIII2: View {
    @State var filteredFriends2 = friends2
    var body: some View {
        NavigationAPIII(view: View3(filteredFriends2: $filteredFriends2),onSearch: {(txt) in
        
        if txt != ""{
            self.filteredFriends2 = friends2.filter{$0.Name.lowercased().contains(txt.lowercased())}
        }
        else{
            self.filteredFriends2 = friends2
        }
    }, onCancel: {
        self.filteredFriends2 = friends2
    })
    .ignoresSafeArea()
    }
}
struct APIII2_Previews: PreviewProvider {
    static var previews: some View {
        APIII2()
    }
}
