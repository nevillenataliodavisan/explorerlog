//
//  CardView2.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 03/05/21.
//

import SwiftUI

struct CardView2: View {
    var data: userData
    var body: some View {
        Button(action:{}){
        VStack{
            
        HStack(spacing: 15){
                Image(data.image)
                    .resizable()
                    .frame(width: 125, height: 175)
                    
            VStack{
                
                HStack{
                    
                    VStack(alignment: .leading, spacing: 5){
                        
                         //   Text("\(data.name)\nExpertise: \(data.expertise)\nShift: \(data.shift)\nInterest: \(data.interest)\nLine: \(data.line)\nWA: \(data.wa)\nInstagram: \(data.ig)")
                         //       .font(.title2)
                       Text("\(data.name)")
                        .font(.title2)
                        
                        Text("""
                            Expertise: \(data.expertise)
                            Shift: \(data.shift)
                            Interest: \(data.interest)
                            Line: \(data.line)
                            WA: \(data.wa)
                            Instagram: \(data.ig)
                            \(data.approachable)
                            """)
                            .font(.caption)
                            .frame(width:125, height:120)
                            
                        
                    }
                    Spacer(minLength: 10)
                    
                    VStack{
                        
                            Image(systemName: "chevron.right")
                                    .font(.body)
                            
                        
                    }
                    
                    }
               
               
                }
           
            }
            Divider()
        }
    }
        .frame(width:370)
        .foregroundColor(Color.black)
        .background(Color.white)
        .cornerRadius(8)
        
    }
}

