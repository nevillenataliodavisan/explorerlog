//
//  CardView.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 28/04/21.
//

import SwiftUI

struct CardView: View {
    var data: userData
    @State private var showModal = false
    var body: some View {
        Button(action:{
            self.showModal.toggle()
        }){
        VStack{
            
        HStack(spacing: 15){
            Image(data.image)
                    .resizable()
                    .frame(width: 125, height: 175)
                    .cornerRadius(8)
                    
            VStack{
                
                HStack{
                    
                    VStack(alignment: .leading, spacing: 5){
                        
                         //   Text("\(data.name)\nExpertise: \(data.expertise)\nShift: \(data.shift)\nInterest: \(data.interest)\nLine: \(data.line)\nWA: \(data.wa)\nInstagram: \(data.ig)")
                         //       .font(.title2)
                       Text("\(data.name)")
                        .font(.title2)
                        
                        Text("""
                            Expertise: \(data.expertise)
                            Shift: \(data.shift)
                            Interest: \(data.interest)
                            Line: \(data.line)
                            WA: \(data.wa)
                            Instagram: \(data.ig)
                            \(data.approachable)
                            """)
                            .font(.caption)
                            .frame(width:125, height:120)
                            
                        
                    }
                    Spacer(minLength: 10)
                    
                    VStack{
                        
                            Image(systemName: "chevron.right")
                                    .font(.body)
                            Spacer()
                        VStack{
                            Button(action:{
                                print(data)
                                acquaintances.append(data)
                            }){
                                Image(systemName: "person.crop.circle.badge.checkmark")
                                    .resizable()
                                    .frame(width: 35, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            }
                            Button(action:{
                                print(data)
                                friends.append(data)
                            }){
                                Image(systemName: "person.2.fill")
                                    .resizable()
                                    .frame(width: 35, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            }
                        }
                    }
                    
                    
                    }
               
               
                }
           
            }
            Divider()
        }
        }.sheet(isPresented: $showModal) {
  //         ModalDetails(aboutMee: self.data.aboutMe)
        }
        .frame(width:370)
        .foregroundColor(Color.black)
        .background(Color.white)
        .cornerRadius(8)
        
    }
}


