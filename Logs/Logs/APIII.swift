//
//  APIII.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 05/05/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct APIII :View{
    var i : Info
    @State private var showModal = false
 //   @Binding var filteredfriends2 : [Info]

    var body:some View{
        
    Button(action:{
        self.showModal.toggle()
    }){
    VStack{
        
    HStack(spacing: 15){
        if i.Photo != ""{
            WebImage(url: URL(string: i.Photo))
                .resizable()
                .scaledToFill()
                .frame(width: 120, height:170)
                .cornerRadius(10)
                .background(Color.secondary)
        }else {
            Text("loader")
        }
        VStack{
            
            HStack{
                
                VStack(alignment: .leading, spacing: 5){
                    Text("\(i.Name)")
                     .font(.title2)
                     
                     Text("""
                         Expertise: \(i.Expertise)
                         Shift: \(i.Shift)
                         Team: \(i.Team)
                         """)
                         .font(.caption)
                         .frame(width:125, height:120)
                    
                }
                Spacer(minLength: 10)
                
                VStack{
                    
                        Image(systemName: "chevron.right")
                                .font(.body)
                        Spacer()
                    VStack{
                        Button(action:{
                            strangers.append(i)
                        }){
                            Image(systemName: "person.crop.circle.badge.xmark")
                                .resizable()
                                .frame(width: 35, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                .foregroundColor(Color.orange)
                        }
                        Button(action:{
                            acquaintances2.append(i)
                        }){
                            Image(systemName: "person.crop.circle.badge.checkmark")
                                .resizable()
                                .frame(width: 35, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                .foregroundColor(Color.orange)
                        }
                    }
                }
                
                
                }
           
           
            }
       
        }
        Divider()
    }
    }.sheet(isPresented: $showModal) {
        ModalDetails(aboutMee: i.aboutMe, name: i.Name, expertise: i.Expertise, interest: i.interest, shift: i.Shift, line: i.line, wa: i.wa, ig: i.ig, approachable: i.approachable)
   }
    .frame(width:370)
    .foregroundColor(Color.black)
    .background(Color.white)
    .cornerRadius(8)
    
    }
}
