//
//  CustomNavigationView3.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 03/05/21.
//

import SwiftUI

struct CustomNavigationView3: UIViewControllerRepresentable{
    func makeCoordinator() -> Coordinator {
        return CustomNavigationView3.Coordinator(parent: self)
    }
    
    
    var view: Friends
    
    var onSearch: (String)->()
    var onCancel: ()->()
    
    init(view: Friends,onSearch: @escaping(String)->(),onCancel: @escaping ()->()) {
        self.view = view
        self.onSearch = onSearch
        self.onCancel = onCancel
        
    }
    
    func makeUIViewController(context: Context) -> UINavigationController {
        
        let childView = UIHostingController(rootView: view)
        
        let controller = UINavigationController(rootViewController: childView)
        
        controller.navigationBar.topItem?.title = "Friends"
        controller.navigationBar.prefersLargeTitles = true
        
        let searchController = UISearchController()
        searchController.searchBar.placeholder = "Name"
        searchController.searchBar.delegate = context.coordinator
        
        searchController.obscuresBackgroundDuringPresentation = false
        controller.navigationBar.topItem?.hidesSearchBarWhenScrolling = false
        controller.navigationBar.topItem?.searchController = searchController
        return controller
    }
    func updateUIViewController(_ uiViewController: UINavigationController, context: Context) {
        
    }
    
    class Coordinator: NSObject,UISearchBarDelegate{
        
        var parent: CustomNavigationView3
        init(parent:CustomNavigationView3){
            self.parent = parent
        }
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText:String){
            self.parent.onSearch(searchText)
        }
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            self.parent.onCancel()
        }
    }
    
}
