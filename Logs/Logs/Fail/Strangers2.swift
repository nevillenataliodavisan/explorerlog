//
//  Strangers2.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 28/04/21.
//

import SwiftUI

struct Strangers2: View {
    @State var filteredUser = users
    var body: some View {
        NavigationView{

        CustomNavigationView(view: Strangers1(filteredUser: $filteredUser),onSearch: {(txt) in
            
            if txt != ""{
                self.filteredUser = users.filter{$0.name.lowercased().contains(txt.lowercased())}
            }
            else{
                self.filteredUser = users
            }
        }, onCancel: {
            self.filteredUser = users
        })
        .ignoresSafeArea()
        .toolbar{
            ToolbarItemGroup(placement: .navigationBarTrailing){
                
                NavigationLink(destination: Myself()) {
                    VStack{
                        Image(systemName: "person.crop.circle")
                    Text("Profile")
                    }
                }
            }
            
        }.navigationBarHidden(false)
    }
}
}
struct Strangers2_Previews: PreviewProvider {
    static var previews: some View {
        Strangers2()
    }
}

