//
//  Acquaintances.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 27/04/21.
//

import SwiftUI

struct Acquaintances: View {
    @Binding var filteredAcquaintances : [userData]
   
    var body: some View {
        ScrollView(.vertical, showsIndicators: false){
            VStack(spacing: 15){
            
            
                ForEach(filteredAcquaintances){data in
                
                    CardView1(data: data)
                }
            }
        
        }
    }
}
