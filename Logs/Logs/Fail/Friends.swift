//
//  Friends.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 27/04/21.
//

import SwiftUI

struct Friends: View {
    @Binding var filteredFriends : [userData]
   
    var body: some View {
        ScrollView(.vertical, showsIndicators: false){
            VStack(spacing: 15){
            
            
                ForEach(filteredFriends){data in
                
                    CardView2(data: data)
                }
            }
        
        }
    }
}
