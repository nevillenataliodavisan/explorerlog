//
//  Strangers1.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 28/04/21.
//

import SwiftUI

struct Strangers1: View {
    @Binding var filteredUser : [userData]
    var body: some View {
        
            ScrollView(.vertical, showsIndicators: false){
                VStack(spacing: 15){
                
                
                    ForEach(filteredUser){data in
                    
                        CardView(data: data)
                        
                     
                    }
                }
            
            }
        }
}

