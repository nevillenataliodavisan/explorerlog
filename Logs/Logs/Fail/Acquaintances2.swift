//
//  Acquaintances2.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 29/04/21.
//

import SwiftUI

struct Acquaintances2: View {
    @State var filteredAcquaintances = acquaintances
    var body: some View {
        NavigationView{

            CustomNavigationView2(view: Acquaintances(filteredAcquaintances: $filteredAcquaintances),onSearch: {(txt) in
            
            if txt != ""{
                self.filteredAcquaintances = acquaintances.filter{$0.name.lowercased().contains(txt.lowercased())}
            }
            else{
                self.filteredAcquaintances = acquaintances
            }
        }, onCancel: {
            self.filteredAcquaintances = acquaintances
        })
        .ignoresSafeArea()
        .toolbar{
            ToolbarItemGroup(placement: .navigationBarTrailing){
                
                NavigationLink(destination: Myself()) {
                    VStack{
                        Image(systemName: "person.crop.circle")
                    Text("Profile")
                    }
                }
            }
            
        }.navigationBarHidden(false)
    }
}
}

struct Acquaintances2_Previews: PreviewProvider {
    static var previews: some View {
        Acquaintances2()
    }
}
