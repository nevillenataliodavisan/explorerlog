//
//  Friends2.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 03/05/21.
//

import SwiftUI

struct Friends2: View{
    @State var filteredFriends = friends
    var body: some View {
        NavigationView{

            CustomNavigationView3(view: Friends(filteredFriends: $filteredFriends),onSearch: {(txt) in
            
            if txt != ""{
                self.filteredFriends = friends.filter{$0.name.lowercased().contains(txt.lowercased())}
            }
            else{
                self.filteredFriends = friends
            }
        }, onCancel: {
            self.filteredFriends = friends
        })
        .ignoresSafeArea()
        .toolbar{
            ToolbarItemGroup(placement: .navigationBarTrailing){
                
                NavigationLink(destination: Myself()) {
                    VStack{
                        Image(systemName: "person.crop.circle")
                    Text("Profile")
                    }
                }
            }
            
        }.navigationBarHidden(false)
    }
}
}
struct Friends2_Previews: PreviewProvider {
    static var previews: some View {
        Friends2()
    }
}
