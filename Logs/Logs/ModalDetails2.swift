//
//  ModalDetails2.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 06/05/21.
//

import SwiftUI

struct ModalDetails2: View {
    @State var question1 :String
    @State var answer1 :String
    @State var answer: String
    var body: some View {
        NavigationView{
        ScrollView(.vertical, showsIndicators: false){
            VStack(alignment:.leading){
                    
                
                    Text("Answer this question")
                        .font(.title2)
                        .fontWeight(.bold)

                    Text("\(question1)")
                        .font(.body)
                Spacer()
                    Text("Answer:")
                        .font(.body)
                        .fontWeight(.bold)
                TextField("write your answer", text: $answer)
                        .font(.body)
                        .cornerRadius(8)
                    
                    }
            
        }.padding()
        .navigationTitle(Text("Question"))
        }
    }
        
    }
