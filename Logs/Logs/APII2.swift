//
//  APII2.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 05/05/21.
//

import SwiftUI

struct APII2: View {
    @State var filteredAcquaintances2 = acquaintances2
    var body: some View {
 //       NavigationView{

            NavigationAPII(view: View2(filteredAcquaintances2: $filteredAcquaintances2),onSearch: {(txt) in
            
            if txt != ""{
                self.filteredAcquaintances2 = acquaintances2.filter{$0.Name.lowercased().contains(txt.lowercased())}
            }
            else{
                self.filteredAcquaintances2 = acquaintances2
            }
        }, onCancel: {
            self.filteredAcquaintances2 = acquaintances2
        })
        .ignoresSafeArea()
 //       .toolbar{
   //         ToolbarItemGroup(placement: .navigationBarTrailing){
                
     //           NavigationLink(destination: Myself()) {
       //             VStack{
         //               Image(systemName: "person.crop.circle")
           //         Text("Profile")
             //       }
               // }
           // }
            
      //  }.navigationBarHidden(false)
    }
}
//}

struct APII2_Previews: PreviewProvider {
    static var previews: some View {
        APII2()
    }
}
