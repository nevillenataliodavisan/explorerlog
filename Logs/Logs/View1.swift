//
//  View1.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 06/05/21.
//

import SwiftUI

struct View1: View {
    @ObservedObject var Infos = InfoModel()
    @Binding var filteredInfo : [Info]
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false){
            VStack(spacing: 15){
                ForEach(filteredInfo){i in
                        
                    API(i: i)

                }
                
            }
        
        }
    }
}
