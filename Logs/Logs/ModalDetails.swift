//
//  ModalDetails.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 30/04/21.
//

import SwiftUI

struct ModalDetails: View {
    @State var aboutMee:String
    @State var name:String
    @State var expertise:String
    @State var interest : String
    @State var shift: String
    @State var line : String
    @State var wa: String
    @State var ig :String
    @State var approachable: Int
    var body: some View {
        NavigationView{
        ScrollView(.vertical, showsIndicators: false){
            VStack(alignment:.leading){
                    
                
                    Text("\(name)")
                        .font(.title2)
                        .fontWeight(.bold)

                    Text("""
                        Expertise : \(expertise)
                        Interest: \(interest)
                        Shift: \(shift)
                        Line: \(line)
                        Wa: \(wa)
                        Ig: \(ig)
                        Approachable: \(approachable)
                        """)
                        .font(.caption)
                Spacer()
                    Text("About Me:")
                        .font(.body)
                        .fontWeight(.bold)
                    Text("\(aboutMee)")
                        .font(.caption)
                        .cornerRadius(8)
                    
                    }
            
        }.padding()
        .navigationTitle(Text("Details"))
        }
    }
        
    }

