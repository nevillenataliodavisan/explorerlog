//
//  InfoModel.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 04/05/21.
//

import Foundation
import Combine
import SwiftyJSON
class InfoModel:ObservableObject{
    @Published var data = [Info]()
    
    init(){
        let url = "https://nc2.theideacompass.com/explorers-api.json"
        let session = URLSession(configuration: .default)
        
        session.dataTask(with: URL(string: url)!){(data, _, err)in
            if err != nil {
                print((err?.localizedDescription))
                return
            }
            
            let json = try! JSON(data:data!)
            
            let items = json[].array!
            
            for i in items{
                let Name = i["Name"].stringValue
                let Expertise = i["Expertise"].stringValue
                let Photo = i["Photo"].stringValue
                let Team = i["Team"].stringValue
                let Shift = i["Shift"].stringValue
                
                DispatchQueue.main.async{
                    strangers.append(Info(Name: Name, Expertise: Expertise, Team: Team, Shift: Shift, Photo: Photo))
                }
            }
        }.resume()
    }
}
