//
//  User.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 04/05/21.
//

import Foundation

struct Info: Identifiable, Equatable{
    var id = UUID()
    var Name:String
    var Expertise:String
    var Team:String
    var Shift:String
    var Photo:String
    var interest : String = ""
    var line : String = ""
    var wa: String = ""
    var ig :String = ""
    var approachable: Int = 10
    var aboutMe :String = "My name is James Willer. I graduated from MIT a month ago. Now, I am looking for my first full-time job. I have been working as a freelance web developer for the last three years and cooperated with several startups. I cannot say with 100% confidence where I see myself in five years or what my big career goal is. The world is changing rapidly, and I don’t have enough professional experience to be certain about such things. However, I know that I can provide a fresh take and an extraordinary approach to every project. I am always open to challenges and constructive feedback. I am open-minded and learn new things quickly. I want to become a team player and dedicate all my skills and talents to develop high-quality and unique products."
    var question1 = "What's my favorite food"
    var answer1 = "Halo"
    var answer = ""
}

var strangers = [Info]()
var acquaintances2 = [Info]()
var friends2 = [Info]()

struct Index{
    var index :Int = 0
    mutating func indexCounter(){
       index += 1
    }
}
