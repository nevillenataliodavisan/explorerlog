//
//  User Data.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 27/04/21.
//

import SwiftUI

struct userData: Identifiable{
    
    var id = UUID()
    var name : String
    var image : String
    var interest : String
    var shift: String
    var expertise:String
    var line : String
    var wa: String
    var ig :String
    var approachable: Int
    var aboutMe :String
}

var users = [
    
   userData(name: "Neville", image: "DOG", interest: "Neville", shift: "Neville", expertise: "Neville", line: "Neville", wa: "Neville", ig: "Neville", approachable: 1, aboutMe: "Halo"),
    userData(name: "Natalio", image: "DOG", interest: "Natalio", shift: "Natalio", expertise: "Natalio", line: "Natalio", wa: "Natalio", ig: "Natalio", approachable: 2, aboutMe: "What"),
    userData(name: "Davisan", image: "DOG", interest: "Davisan", shift: "Davisan", expertise: "Davisan", line: "Davisan", wa: "Davisan", ig: "Davisan", approachable: 3, aboutMe: "WOT")
]

var acquaintances = [userData]()
//                userData(name: "HALO", image: "DOG", interest: "HALO", shift: "HALO", expertise: "HALO??", line: "HALO??", wa: "HALO??", ig: "HALO??", approachable: 1, aboutMe: "Halo"),
//                 userData(name: "NAT", image: "DOG", interest: "NAT", shift: "NAT", expertise: "NAT", line: "NAT", wa: "NAT", ig: "NAT", approachable: 2, aboutMe: "Halo"),
//                 userData(name: "Dab", image: "DOG", interest: "Dab", shift: "Dab", expertise: "Dab", line: "Dab", wa: "Dab", ig: "Dab", approachable: 3, aboutMe: "Halo")]

var friends = [userData]()
//               userData(name: "No", image: "DOG", interest: "No??", shift: "No??", expertise: "No??", line: "No??", wa: "No??", ig: "No??", approachable: 1, aboutMe: "Halo"),
 //              userData(name: "Yesy", image: "DOG", interest: "Yesy??", shift: "Yesy??", expertise: "Yesy??", line: "Yesy??", wa: "Yesy??", ig: "Yesy??", approachable: 2, aboutMe: "Yesy??"),
 //              userData(name: "DOnt", image: "DOG", interest: "DOnt", shift: "DOnt", expertise: "DOnt", line: "DOnt", wa: "DOnt", ig: "DOnt", approachable: 3, aboutMe: "Halo")]
