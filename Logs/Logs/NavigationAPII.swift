//
//  NavigationAPII.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 05/05/21.
//

import SwiftUI

struct NavigationAPII: UIViewControllerRepresentable{
    func makeCoordinator() -> Coordinator {
        return NavigationAPII.Coordinator(parent: self)
    }
    
    
    var view: View2
    
    var onSearch: (String)->()
    var onCancel: ()->()
    
    init(view: View2,onSearch: @escaping(String)->(),onCancel: @escaping ()->()) {
        self.view = view
        self.onSearch = onSearch
        self.onCancel = onCancel
        
    }
    
    func makeUIViewController(context: Context) -> UINavigationController {
        
        let childView = UIHostingController(rootView: view)
        
        let controller = UINavigationController(rootViewController: childView)
        
        controller.navigationBar.topItem?.title = "Acquaintances􀉳"
        controller.navigationBar.prefersLargeTitles = true

        let searchController = UISearchController()
        searchController.searchBar.placeholder = "Name"
        searchController.searchBar.delegate = context.coordinator
        
        searchController.obscuresBackgroundDuringPresentation = false
        controller.navigationBar.topItem?.hidesSearchBarWhenScrolling = false
        controller.navigationBar.topItem?.searchController = searchController
        return controller
    }
    func updateUIViewController(_ uiViewController: UINavigationController, context: Context) {
        
    }
    
    class Coordinator: NSObject,UISearchBarDelegate{
        
        var parent: NavigationAPII
        init(parent:NavigationAPII){
            self.parent = parent
        }
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText:String){
            self.parent.onSearch(searchText)
        }
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            self.parent.onCancel()
        }
    }
    
}
