//
//  LogsApp.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 27/04/21.
//

import SwiftUI

@main
struct LogsApp: App {
    var body: some Scene {
        WindowGroup {
            TabBar()
        }
    }
}
