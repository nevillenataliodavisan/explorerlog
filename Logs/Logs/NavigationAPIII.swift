//
//  NavigationAPIII.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 05/05/21.
//

import SwiftUI

struct NavigationAPIII: UIViewControllerRepresentable{
    func makeCoordinator() -> Coordinator {
        return NavigationAPIII.Coordinator(parent: self)
    }
    
    
    var view: View3
    
    var onSearch: (String)->()
    var onCancel: ()->()
    
    init(view: View3,onSearch: @escaping(String)->(),onCancel: @escaping ()->()) {
        self.view = view
        self.onSearch = onSearch
        self.onCancel = onCancel
        
    }
    
    func makeUIViewController(context: Context) -> UINavigationController {
        
        let childView = UIHostingController(rootView: view)
        
        let controller = UINavigationController(rootViewController: childView)
        
        controller.navigationBar.topItem?.title = "Friends􀉬"
        controller.navigationBar.prefersLargeTitles = true

        let searchController = UISearchController()
        searchController.searchBar.placeholder = "Name"
        searchController.searchBar.delegate = context.coordinator
        
        searchController.obscuresBackgroundDuringPresentation = false
        controller.navigationBar.topItem?.hidesSearchBarWhenScrolling = false
        controller.navigationBar.topItem?.searchController = searchController
        return controller
    }
    func updateUIViewController(_ uiViewController: UINavigationController, context: Context) {
        
    }
    
    class Coordinator: NSObject,UISearchBarDelegate{
        
        var parent: NavigationAPIII
        init(parent:NavigationAPIII){
            self.parent = parent
        }
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText:String){
            self.parent.onSearch(searchText)
        }
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            self.parent.onCancel()
        }
    }
    
}

