//
//  TabBar.swift
//  Logs
//
//  Created by Neville Natalio Davisan on 27/04/21.
//

import SwiftUI

struct TabBar: View {
    var body: some View {
            TabView{
               API2()
                    .tabItem {
                        Image(systemName: "person.crop.circle.badge.xmark")
                        Text("Strangers")
                            
                    }
                APII2()
                    .tabItem{
                        Image(systemName: "person.crop.circle.badge.checkmark")
                        Text("Acquaintances")
                    }
                APIII2()
                    .tabItem{
                        Image(systemName: "person.2.fill")
                        Text("Friends")
                    }
                Myself()
                   .tabItem {
                       Image(systemName: "person.fill")
                      Text("Myself")
                 }
            }
            .accentColor(.orange)
    }
}


struct TabBar_Previews: PreviewProvider {
    static var previews: some View {
        TabBar()
    }
}
